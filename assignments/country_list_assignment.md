# Counties list Assignment


1. Create UI as per this design: https://xd.adobe.com/view/0366b032-b6f8-423a-738e-09d445f7279d-7557/
2. There will be 2 web screens
    1. Country list screen
    2. Country detail screen
3. Get the country list using country list API: https://restcountries.eu/rest/v2/all
4. On country list screen, add search bar as per the UI, which will search the country by name only.
5. On click of "Show Map" open the google map for the country.
6. On click of "Detail" navigate the user to 'Country Detail' screen and show the country detail as per UI.
7. Launch the developed appication on [Heroku](https://www.heroku.com/) or [Netlify](https://www.netlify.com/)


Technology: [NextJS](https://nextjs.org/)

API Enpoints: https://restcountries.eu/#api-endpoints-all
